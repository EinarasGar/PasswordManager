package com.example.passwordmanager;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbConstants;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbEndpoint;
import android.hardware.usb.UsbInterface;
import android.hardware.usb.UsbRequest;
import android.os.BatteryManager;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.hardware.usb.UsbManager;
import android.hardware.usb.UsbDevice;
import android.content.Context;

import androidx.annotation.NonNull;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.util.HashMap;

import io.flutter.Log;
import io.flutter.embedding.android.FlutterActivity;
import io.flutter.embedding.engine.FlutterEngine;
import io.flutter.embedding.engine.plugins.FlutterPlugin.FlutterPluginBinding;
import io.flutter.plugin.common.MethodChannel;

public class MainActivity extends FlutterActivity {
  private static final String CHANNEL = "samples.flutter.dev/battery";
    private static String ACTION_USB_PERMISSION = "com.android.example.USB_PERMISSION";

    private final BroadcastReceiver usbReceiver = new BroadcastReceiver() {

        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (ACTION_USB_PERMISSION.equals(action)) {
                synchronized (this) {
                    UsbDevice device = (UsbDevice)intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);

                    if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false))
                    {
                        System.out.println("Permission granted");
                        if (device != null)
                        {
                            // call method to set up device communication
                        }
                    }
                    else
                    {
                        System.out.println("Permission denied");

                    }
                }
            }
        }
    };

    private static final int HID_BUFFER_SIZE = 64;
    private static final int LEDGER_DEFAULT_CHANNEL = 1;
    private static final int SW1_DATA_AVAILABLE = 0x61;


    @Override
  public void configureFlutterEngine(@NonNull FlutterEngine flutterEngine) {
    super.configureFlutterEngine(flutterEngine);
    new MethodChannel(flutterEngine.getDartExecutor().getBinaryMessenger(), CHANNEL)
        .setMethodCallHandler(
            (call, result) -> {
              // Note: this method is invoked on the main thread.
              if (call.method.equals("sendToLedger")) {
                  
                byte [] command = (byte[])call.arguments;

                Context context = getApplicationContext();
                UsbManager manager = (UsbManager) getSystemService(context.USB_SERVICE);
                HashMap<String, UsbDevice> usbDevices = manager.getDeviceList();

                UsbDevice ledgerNanoS = null;
                for (String key : usbDevices.keySet()) {
                    UsbDevice device = usbDevices.get(key);
                    if(device.getVendorId() == 11415 && device.getProductId() == 4113) {
                        ledgerNanoS = device;
                        System.out.println("Found Ledger Nano S");
                    }
                }

                if(ledgerNanoS == null){
                  System.out.println("USB device not found");
                }

                if(manager.hasPermission(ledgerNanoS)){
                    System.out.println("USB permissions got");

                    UsbInterface dongleInterface = ledgerNanoS.getInterface(0);
                    UsbEndpoint in = null;
                    UsbEndpoint out = null;
                    for (int i = 0; i < dongleInterface.getEndpointCount(); i++) {
                        UsbEndpoint tmpEndpoint = dongleInterface.getEndpoint(i);
                        if (tmpEndpoint.getDirection() == UsbConstants.USB_DIR_IN) {
                            in = tmpEndpoint;
                        } else {
                            out = tmpEndpoint;
                        }
                    }
                    UsbDeviceConnection connection = manager.openDevice(ledgerNanoS);
                    connection.claimInterface(dongleInterface, true);

                    byte[] transferBuffer = new byte[HID_BUFFER_SIZE];
                    try {
                        ByteArrayOutputStream response = new ByteArrayOutputStream();
                        byte[] responseData = null;
                        int offset = 0;
                        int responseSize;

                        UsbRequest request = new UsbRequest();
                        if (!request.initialize(connection, out)) {
                            throw new Exception("I/O error");
                        }
                        while (offset != command.length) {
                            int blockSize = (command.length - offset > HID_BUFFER_SIZE ? HID_BUFFER_SIZE : command.length - offset);
                            System.arraycopy(command, offset, transferBuffer, 0, blockSize);
                            if (!request.queue(ByteBuffer.wrap(transferBuffer), HID_BUFFER_SIZE)) {
                                request.close();
                                throw new Exception("I/O error");
                            }
                            connection.requestWait();
                            offset += blockSize;
                        }
                        ByteBuffer responseBuffer = ByteBuffer.allocate(HID_BUFFER_SIZE);
                        request = new UsbRequest();
                        if (!request.initialize(connection, in)) {
                            request.close();
                            throw new Exception("I/O error");
                        }

                        while((response.toByteArray() == null) || (response.toByteArray().length < 7 + 5)){
                            responseBuffer.clear();
                            if (!request.queue(responseBuffer, HID_BUFFER_SIZE)) {
                                request.close();
                                throw new Exception("I/O error");
                            }
                            connection.requestWait();
                            responseBuffer.rewind();
                            responseBuffer.get(transferBuffer, 0, HID_BUFFER_SIZE);
                            response.write(transferBuffer, 0, HID_BUFFER_SIZE);
                        }

                        request.close();
                        result.success(response.toByteArray());
                    } catch (Exception e) {
                        e.printStackTrace();
                        result.error("ERROR","Unknwn exception",e);
                    }

                } else {

                    PendingIntent mPermissionIntent = PendingIntent.getBroadcast(context, 0, new Intent(ACTION_USB_PERMISSION), PendingIntent.FLAG_MUTABLE);
                    IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
                    registerReceiver(usbReceiver, filter);

                    System.out.println("Getting USB permissions");
                    manager.requestPermission(ledgerNanoS, mPermissionIntent);
                }
              } else {
                result.notImplemented();
              }
            }

        );
  }


    public static byte[] unwrapResponseAPDU(int channel, byte[] data, int packetSize) throws Exception {
        ByteArrayOutputStream response = new ByteArrayOutputStream();
        int offset = 0;
        int responseLength;
        int sequenceIdx = 0;
        if ((data == null) || (data.length < 7 + 5)) {
            System.out.println("Returning null 0");
            return null;
        }
        if (data[offset++] != (channel >> 8)) {
            System.out.println("Invalid channel");
        }
        if (data[offset++] != (channel & 0xff)) {
            System.out.println("Invalid channel");
        }
        if (data[offset++] != 0x05) {
            System.out.println("Invalid tag");
        }
        if (data[offset++] != 0x00) {
            System.out.println("Invalid sequence");
        }
        if (data[offset++] != 0x00) {
            System.out.println("Invalid sequence");
        }
        responseLength = ((data[offset++] & 0xff) << 8);
        responseLength |= (data[offset++] & 0xff);
        if (data.length < 7 + responseLength) {
            System.out.println("Returning null 1");
            return null;
        }
        int blockSize = (responseLength > packetSize - 7 ? packetSize - 7 : responseLength);
        response.write(data, offset, blockSize);
        offset += blockSize;
        while (response.size() != responseLength) {
            sequenceIdx++;
            if (offset == data.length) {
                return null;
            }
            if (data[offset++] != (channel >> 8)) {
                System.out.println("Invalid channel");
            }
            if (data[offset++] != (channel & 0xff)) {
                System.out.println("Invalid channel");
            }
            if (data[offset++] != 0x05) {
                System.out.println("Invalid tag");
            }
            if (data[offset++] != (sequenceIdx >> 8)) {
                System.out.println("Invalid sequence");
            }
            if (data[offset++] != (sequenceIdx & 0xff)) {
                System.out.println("Invalid sequence");
            }
            blockSize = (responseLength - response.size() > packetSize - 5 ? packetSize - 5 : responseLength - response.size());
            if (blockSize > data.length - offset) {
                System.out.println("Returning null 2");
                return null;
            }
            response.write(data, offset, blockSize);
            offset += blockSize;
        }
        return response.toByteArray();
    }
}

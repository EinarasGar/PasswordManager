import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:hive/hive.dart';

import 'logic/hardware_communcation.dart';
import 'logic/hardware_communication_exceptions.dart';
import 'logic/password_strength.dart';
import 'models/password_entry.dart';
import 'package:zxcvbn/zxcvbn.dart';

class AddPasswordPage extends StatefulWidget {
  const AddPasswordPage({Key? key}) : super(key: key);

  @override
  State<AddPasswordPage> createState() => _AddPasswordPageState();
}

class _AddPasswordPageState extends State<AddPasswordPage> {
  bool isChecked = false;

  Color passwordFieldColor = const Color.fromARGB(255, 255, 0, 0);
  String passwordHint = "";

  Future<void> _savePassword() async {
    HardwareCommuncation nations = HardwareCommuncation();
    Uint8List response = Uint8List(0);

    try {
      if (isChecked) {
        await nations.savePassword(websiteField.text, usernameField.text, passwordField.text);
      } else {
        response = await nations.encryptPassword(passwordField.text);
      }
    } on LedgerNotFoundException {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text('Your ledger device is not connected.')));
      return;
    } on LedgerNoSpaceLeftException {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text('No space left to store the password.')));
      return;
    } catch (e) {
      return;
    }

    if (!isChecked) {
      final entry = PasswordEntry()
        ..website = websiteField.text
        ..category = ""
        ..username = usernameField.text
        ..url = webisteUrLField.text
        ..password = response;

      final passwordsBox = Hive.box<PasswordEntry>('passwords');
      passwordsBox.add(entry);
    }

    if (isChecked) {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text('Password was successfully saved to the device.')));
    } else {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text('Password was successfully saved.')));
    }
    Navigator.pop(context, false);
  }

  final websiteField = TextEditingController();
  final usernameField = TextEditingController();
  final passwordField = TextEditingController();
  // final catergoryField = TextEditingController();
  final webisteUrLField = TextEditingController();

  @override
  void dispose() {
    // Clean up the controllers when the widget is disposed.
    websiteField.dispose();
    usernameField.dispose();
    passwordField.dispose();
    // catergoryField.dispose();
    webisteUrLField.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: true,
        title: const Text('Add password'),
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () => Navigator.pop(context, false),
        ),
      ),
      body: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Card(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    TextFormField(
                      decoration: const InputDecoration(
                        icon: Icon(Icons.language),
                        labelText: 'Website',
                      ),
                      controller: websiteField,
                    ),
                    TextFormField(
                      decoration: const InputDecoration(
                        icon: Icon(Icons.badge),
                        labelText: 'Username',
                      ),
                      controller: usernameField,
                    ),
                    TextFormField(
                      decoration: InputDecoration(
                        icon: Icon(Icons.vpn_key),
                        labelText: 'Password',
                        labelStyle: passwordField.text != ""
                            ? TextStyle(
                                color: passwordFieldColor,
                              )
                            : null,
                        helperText: passwordHint,
                        enabledBorder: passwordField.text != ""
                            ? UnderlineInputBorder(
                                borderSide: BorderSide(
                                  color: passwordFieldColor,
                                ),
                              )
                            : null,
                        suffixIcon: IconButton(
                          icon: const Icon(Icons.refresh),
                          onPressed: () {
                            setState(() {
                              passwordField.text = generateSecurePassword();
                              final strength = getPasswordStrength(passwordField.text);
                              passwordFieldColor = strength.item1;
                              passwordHint = strength.item2;
                            });
                          },
                        ),
                      ),
                      controller: passwordField,
                      onChanged: (value) {
                        setState(() {
                          final strength = getPasswordStrength(value);
                          passwordFieldColor = strength.item1;
                          passwordHint = strength.item2;
                        });
                      },
                    ),
                    // TextFormField(
                    //   decoration: const InputDecoration(
                    //     icon: Icon(Icons.category),
                    //     labelText: 'Category',
                    //   ),
                    //   controller: catergoryField,
                    // ),
                    TextFormField(
                      decoration: const InputDecoration(
                        icon: Icon(Icons.link),
                        labelText: 'Website url',
                      ),
                      controller: webisteUrLField,
                    ),
                    CheckboxListTile(
                        value: isChecked,
                        title: const Text('Save password to devices memory.'),
                        onChanged: (bool? value) {
                          setState(() {
                            isChecked = value!;
                          });
                        })
                  ],
                ),
              ),
            ),
          ),
          // const Divider(),
          // const Card(child: Trext("TOPT"))
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          _savePassword();
        },
        tooltip: 'Add password',
        child: const Icon(Icons.add),
      ),
    );
  }
}

import 'dart:convert';
import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:permission_handler/permission_handler.dart';

import 'logic/hardware_communcation.dart';
import 'models/password_entry.dart';

import 'package:flutter/foundation.dart' show kIsWeb;
import 'dart:convert' show utf8;

class SideBar extends StatefulWidget {
  @override
  State<SideBar> createState() => _SideBarState();
}

class _SideBarState extends State<SideBar> {
  Future<void> _createBackup() async {
    final passwordsBox = Hive.box<PasswordEntry>('passwords');
    if (passwordsBox.isEmpty) {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('No Passwords Stored.')),
      );
      return;
    }
    ScaffoldMessenger.of(context).showSnackBar(
      const SnackBar(content: Text('Creating backup...')),
    );
    Map<String, dynamic> map = passwordsBox.toMap().map((key, value) => MapEntry(key.toString(), value));
    String json = jsonEncode(map);
    if (Platform.isAndroid) await Permission.storage.request();
    String? selectedDirectory = await FilePicker.platform.getDirectoryPath();

    if (selectedDirectory == null) {
      // User canceled the picker
      return;
    }
    Directory dir = Directory(selectedDirectory);
    String formattedDate = DateTime.now().toString().replaceAll('.', '-').replaceAll(' ', '-').replaceAll(':', '-');
    String path = '${dir.path}$formattedDate.json'; //Change .json to your desired file format(like .barbackup or .hive).
    File backupFile = File(path);
    await backupFile.writeAsString(json);
    ScaffoldMessenger.of(context).showSnackBar(
      const SnackBar(content: Text('Backup saved.')),
    );
  }

  Future<void> _restoreBackup() async {
    if (!kIsWeb) {
      if (Platform.isAndroid) await Permission.storage.request();
    }
    ScaffoldMessenger.of(context).showSnackBar(
      const SnackBar(content: Text('Restoring backup...')),
    );
    FilePickerResult? file = await FilePicker.platform.pickFiles(
      type: FileType.any,
    );
    if (file != null) {
      String fileText;
      if (kIsWeb) {
        fileText = utf8.decode(file.files.single.bytes!);
      } else {
        File files = File(file.files.single.path.toString());
        fileText = await files.readAsString();
      }
      Map<String, dynamic> map = jsonDecode(fileText);
      Hive.box<PasswordEntry>('passwords').clear();
      for (var i = 0; i < map.length; i++) {
        PasswordEntry password = PasswordEntry.fromJson(map.values.elementAt(i));
        Hive.box<PasswordEntry>('passwords').add(password);
      }
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('Restored Successfully...')),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      elevation: 5,
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          const DrawerHeader(
              decoration: BoxDecoration(
                color: Colors.blue,
              ),
              child: null),
          ListTile(
            leading: const Icon(Icons.backup),
            title: const Text('Create backup'),
            onTap: () {
              _createBackup();
            },
          ),
          ListTile(
            leading: const Icon(Icons.backup),
            title: const Text('Restore backup'),
            onTap: () {
              _restoreBackup();
            },
          ),
          ListTile(
            leading: const Icon(Icons.delete_forever),
            title: const Text('Delete all passwords.'),
            onTap: () {
              final passwordsBox = Hive.box<PasswordEntry>('passwords');
              passwordsBox.clear();
            },
          ),
          ListTile(
            leading: const Icon(Icons.verified),
            title: const Text('Check hardware app version.'),
            onTap: () async {
              HardwareCommuncation comm = HardwareCommuncation();
              String version = await comm.checkVersion();
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text('The app version is $version')));
            },
          )
        ],
      ),
    );
  }
}

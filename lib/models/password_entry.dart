import 'dart:typed_data';

import 'package:hive/hive.dart';

part 'password_entry.g.dart';

@HiveType(typeId: 0)
class PasswordEntry extends HiveObject {
  @HiveField(0)
  String? website;

  @HiveField(1)
  String? url;

  @HiveField(2)
  String? username;

  @HiveField(3)
  Uint8List? password;

  @HiveField(4)
  String? category;

  @override
  String toString() {
    return 'PasswordEntry{website: $website, url: $url, username: $username, password: $password, category: $category}';
  }

  Map<String, dynamic> toJson() => {
        'website': website,
        'url': url,
        'username': username,
        'password': password,
        'category': category,
      };

  static PasswordEntry fromJson(Map<String, dynamic> map) {
    List<int> intList =
        map['password'].cast<int>().toList(); //This is the magical line.
    Uint8List data = Uint8List.fromList(intList);

    return PasswordEntry()
      ..website = map['website'] as String?
      ..url = map['url'] as String?
      ..username = map['username'] as String?
      ..password = data
      ..category = map['category'] as String?;
  }
}

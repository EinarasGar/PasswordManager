import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:passwordmanager/logic/clipboard_manager.dart';
import 'package:passwordmanager/models/password_entry.dart';

import 'logic/hardware_communcation.dart';
import 'logic/hardware_communication_exceptions.dart';

import 'package:flutter/services.dart';

import 'logic/password_strength.dart';

class PasswordEntryPage extends StatefulWidget {
  final PasswordEntry password;

  const PasswordEntryPage({Key? key, required this.password}) : super(key: key);

  @override
  State<PasswordEntryPage> createState() => _PasswordEntryPageState();
}

class _PasswordEntryPageState extends State<PasswordEntryPage> {
  final passwordField = TextEditingController();
  final websiteController = TextEditingController();
  final usernameController = TextEditingController();
  final urlController = TextEditingController();

  bool performingOperation = false;
  Color passwordFieldColor = const Color.fromARGB(255, 255, 0, 0);
  String passwordHint = "";
  bool passwordShown = false;
  bool fieldsUpdated = false;
  String decryptedPassword = "";

  Future<void> _displayPassword() async {
    await _decryptPassword(true);
    setState(() {
      performingOperation = false;
    });
  }

  Future<void> _viewPassword() async {
    final decryptedPassword = await _decryptPassword(false) ?? "";
    passwordField.text = decryptedPassword;
    final strength = getPasswordStrength(decryptedPassword);
    passwordFieldColor = strength.item1;
    passwordHint = strength.item2;
    passwordShown = true;
    setState(() {
      performingOperation = false;
    });
  }

  Future<void> _copyPassword() async {
    var password = await _decryptPassword(false) ?? "";
    await CopyToClipboard(password, context, "Password");
    setState(() {
      performingOperation = false;
    });
  }

  Future<void> _copyEmail() async {
    if (widget.password.username == null) return;
    await CopyToClipboard(widget.password.username!, context, "Username");
    setState(() {
      performingOperation = false;
    });
  }

  Future<String?> _decryptPassword(bool showOnDisplay) async {
    setState(() {
      performingOperation = true;
    });

    HardwareCommuncation nations = HardwareCommuncation();

    if (widget.password.password == null) {
      return null;
    }

    try {
      var password = await nations.decryptPassword(widget.password.password!, showOnDisplay);
      decryptedPassword = password;
      return password;
    } on LedgerNotFoundException {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text('Your ledger device is not connected.')));
      return null;
    } on LedgerDisplayedOnScreenException {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text('Your password is being displayed on the device.')));
      return null;
    } catch (e) {
      return null;
    }
  }

  Future<void> _deleteEntry() async {
    final passwordsBox = Hive.box<PasswordEntry>('passwords');
    await passwordsBox.delete(widget.password.key);
    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text("Entry was deleted")));
    Navigator.pop(context, false);
  }

  Future<Uint8List?> _encryptPassword() async {
    HardwareCommuncation nations = HardwareCommuncation();
    Uint8List response = Uint8List(0);

    try {
      response = await nations.encryptPassword(passwordField.text);
      return response;
    } on LedgerNotFoundException {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text('Your ledger device is not connected.')));
      return null;
    } catch (e) {
      return null;
    }
  }

  void checkIfFieldsUpdated() {
    setState(() {
      if (usernameController.text != widget.password.username) {
        fieldsUpdated = true;
        return;
      }
      if (urlController.text != widget.password.url) {
        fieldsUpdated = true;
        return;
      }
      if (websiteController.text != widget.password.website) {
        fieldsUpdated = true;
        return;
      }
      if (passwordShown && passwordField.text != decryptedPassword) {
        fieldsUpdated = true;
        return;
      }
      if (passwordShown == false && passwordField.text != "") {
        fieldsUpdated = true;
        return;
      }
      fieldsUpdated = false;
      return;
    });
  }

  void _updateDetails() async {
    final passwordsBox = Hive.box<PasswordEntry>('passwords');

    widget.password.username = usernameController.text;
    widget.password.url = urlController.text;
    widget.password.website = websiteController.text;

    if ((passwordShown && passwordField.text != decryptedPassword) || (passwordShown == false && passwordField.text != "")) {
      widget.password.password = await _encryptPassword();
    }

    passwordsBox.put(widget.password.key, widget.password);
    Navigator.pop(context, false);
  }

  @override
  void initState() {
    super.initState();
    websiteController.text = widget.password.website!;
    usernameController.text = widget.password.username!;
    urlController.text = widget.password.url!;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: true,
        title: const Text(''),
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () => Navigator.pop(context, false),
        ),
      ),
      body: Column(
        children: <Widget>[
          Visibility(visible: performingOperation, child: const LinearProgressIndicator()),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Card(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    TextFormField(
                      controller: websiteController,
                      // initialValue: widget.password.website,
                      onChanged: (val) {
                        checkIfFieldsUpdated();
                      },
                      decoration: const InputDecoration(
                        icon: Icon(Icons.language),
                        labelText: 'Website',
                      ),
                    ),
                    TextFormField(
                      controller: usernameController,
                      // initialValue: widget.password.username,
                      onChanged: (val) {
                        checkIfFieldsUpdated();
                      },
                      decoration: InputDecoration(
                        icon: const Icon(Icons.badge),
                        labelText: 'Username',
                        suffixIcon: IconButton(
                          icon: const Icon(Icons.content_copy),
                          onPressed: () {
                            _copyEmail();
                          },
                        ),
                      ),
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: TextFormField(
                            onChanged: (val) {
                              checkIfFieldsUpdated();
                            },
                            decoration: InputDecoration(
                              icon: Icon(Icons.vpn_key),
                              labelText: 'Password',
                              labelStyle: passwordShown
                                  ? TextStyle(
                                      color: passwordFieldColor,
                                    )
                                  : null,
                              helperText: passwordHint,
                              enabledBorder: passwordShown
                                  ? UnderlineInputBorder(
                                      borderSide: BorderSide(
                                        color: passwordFieldColor,
                                      ),
                                    )
                                  : null,
                            ),
                            controller: passwordField,
                          ),
                        ),
                        IconButton(
                          icon: const Icon(Icons.monitor),
                          tooltip: 'Display password on the device.',
                          onPressed: () {
                            _displayPassword();
                          },
                        ),
                        IconButton(
                          icon: const Icon(Icons.visibility),
                          tooltip: 'View password.',
                          onPressed: () {
                            _viewPassword();
                          },
                        ),
                        IconButton(
                          icon: const Icon(Icons.copy),
                          tooltip: 'Copy password.',
                          onPressed: () {
                            _copyPassword();
                          },
                        ),
                      ],
                    ),
                    // TextFormField(
                    //   initialValue: widget.password.category,
                    //   readOnly: true,
                    //   decoration: const InputDecoration(
                    //     icon: Icon(Icons.category),
                    //     labelText: 'Category',
                    //   ),
                    // ),
                    TextFormField(
                      // initialValue: widget.password.url,
                      controller: urlController,
                      decoration: const InputDecoration(
                        icon: Icon(Icons.link),
                        labelText: 'Website url',
                      ),
                      onChanged: (val) {
                        checkIfFieldsUpdated();
                      },
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0, 20, 0, 0),
                      child: SizedBox(
                        child: ElevatedButton(
                          onPressed: fieldsUpdated
                              ? () {
                                  _updateDetails();
                                }
                              : null,
                          child: const Text('Update details'),
                        ),
                        width: double.infinity,
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          _deleteEntry();
        },
        tooltip: 'Delete entry',
        child: const Icon(Icons.delete),
      ),
    );
  }
}

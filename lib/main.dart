import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:passwordmanager/addpasswordpage.dart';

import 'package:passwordmanager/logic/hardware_communcation.dart';
import 'package:passwordmanager/logic/hardware_communication_exceptions.dart';
import 'package:passwordmanager/models/password_entry.dart';
import 'package:passwordmanager/passwordentrypage.dart';
import 'package:passwordmanager/sidebar.dart';
import 'package:cached_network_image/cached_network_image.dart';

import 'logic/clipboard_manager.dart';

void main() async {
  await Hive.initFlutter(".passwordmanager");
  Hive.registerAdapter(PasswordEntryAdapter());

  const secureStorage = FlutterSecureStorage();
  final encryprionKey = await secureStorage.read(key: 'key');
  if (encryprionKey == null) {
    final key = Hive.generateSecureKey();
    await secureStorage.write(
      key: 'key',
      value: base64UrlEncode(key),
    );
  }
  final key = await secureStorage.read(key: 'key');
  final encryptionKey = base64Url.decode(key!);
  await Hive.openBox<PasswordEntry>('passwords', encryptionCipher: HiveAesCipher(encryptionKey));
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // Root of the application
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Password Manager',
      theme: ThemeData(primarySwatch: Colors.blue),
      home: const MyHomePage(title: 'Password Manager.'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  late Box<PasswordEntry> passwordsBox;

  bool performingOperation = false;

  Future _copyPassword(Uint8List? password) async {
    if (password == null) {
      return;
    }
    setState(() {
      performingOperation = true;
    });

    HardwareCommuncation nations = HardwareCommuncation();

    try {
      var decryptedPassword = await nations.decryptPassword(password, false);
      await CopyToClipboard(decryptedPassword, context, "Password");
      setState(() {
        performingOperation = false;
      });
    } on LedgerNotFoundException {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text('Your ledger device is not connected.')));
    } on LedgerDisplayedOnScreenException {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text('Your password is being displayed on the device.')));
    }
  }

  Future _copyUsername(String? username) async {
    if (username == null) return;
    await CopyToClipboard(username, context, "Username");
  }

  final _seachController = TextEditingController();

  @override
  void initState() {
    super.initState();
    passwordsBox = Hive.box<PasswordEntry>('passwords');
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    bool isMobileWidth = screenWidth < 600;

    return Scaffold(
      drawer: isMobileWidth ? SideBar() : null,
      body: LayoutBuilder(builder: (context, constraints) {
        if (isMobileWidth) {
          return constructScrollView(isMobileWidth);
        } else {
          return Row(children: [
            SizedBox(
              width: 240,
              child: SideBar(),
            ),
            Expanded(
              child: constructScrollView(isMobileWidth),
            ),
          ]);
        }
      }),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => const AddPasswordPage(),
            ),
          );
        },
        tooltip: 'Add password',
        child: const Icon(Icons.add),
      ),
    );
  }

  CustomScrollView constructScrollView(bool isMobileWidth) {
    return CustomScrollView(
      slivers: <Widget>[
        const SliverAppBar(
          pinned: true,
          snap: false,
          floating: false,
          expandedHeight: 160.0,
          flexibleSpace: FlexibleSpaceBar(
            title: Text('Password Manager'),
            // background: FlutterLogo(),
          ),
        ),
        SliverToBoxAdapter(
          child: Column(
            children: [
              Visibility(visible: performingOperation, child: const LinearProgressIndicator()),
              SizedBox(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextField(
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: 'Search passwords...',
                    ),
                    controller: _seachController,
                    onChanged: (value) {
                      setState(() {});
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
        ValueListenableBuilder(
          valueListenable: Hive.box<PasswordEntry>("passwords").listenable(),
          builder: (context, Box<PasswordEntry> passwordsBox, _) {
            var filteredPasswords = passwordsBox.values
                .where((password) =>
                    password.website!.toLowerCase().contains(_seachController.text.toLowerCase()) ||
                    password.username!.toLowerCase().contains(_seachController.text.toLowerCase()))
                .toList();

            if (passwordsBox.values.isEmpty) {
              return SliverList(
                delegate: SliverChildListDelegate(
                  [
                    const Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Center(child: Text('No passwords found.')),
                    ),
                  ],
                ),
              );
            }
            return SliverList(
              delegate: SliverChildBuilderDelegate(
                (context, index) {
                  final password = filteredPasswords[index];
                  if (password == null) {
                    return Container();
                  }
                  return Padding(
                    padding: !isMobileWidth ? EdgeInsets.fromLTRB(8, 0, 8, 5) : EdgeInsets.fromLTRB(0, 0, 0, 0),
                    child: Card(
                      child: InkWell(
                        splashColor: Colors.blue.withAlpha(30),
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => PasswordEntryPage(
                                password: password,
                              ),
                            ),
                          );
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(children: <Widget>[
                            Padding(
                              padding: isMobileWidth ? EdgeInsets.all(5.0) : EdgeInsets.all(15.0),
                              child: CachedNetworkImage(
                                placeholder: (context, url) => const CircularProgressIndicator(),
                                imageUrl: 'https://www.google.com/s2/favicons?domain=' + password.url!,
                                fit: BoxFit.fill,
                                width: isMobileWidth ? 20 : 25,
                              ),
                            ),
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    password.website ?? "",
                                    style: isMobileWidth ? TextStyle(fontSize: 20) : TextStyle(fontSize: 26),
                                    // Theme.of(context).textTheme.headline5,
                                  ),
                                  Text(
                                    password.username ?? "",
                                    style: isMobileWidth ? TextStyle(fontSize: 16) : TextStyle(fontSize: 20),
                                  ),
                                ],
                              ),
                            ),
                            isMobileWidth
                                ? IconButton(
                                    icon: const Icon(Icons.email_outlined),
                                    tooltip: 'Copy username.',
                                    onPressed: () {
                                      _copyUsername(password.username);
                                    },
                                  )
                                : TextButton(
                                    child: const Text('Copy username'),
                                    onPressed: () {
                                      _copyUsername(password.username);
                                    },
                                  ),
                            isMobileWidth
                                ? IconButton(
                                    icon: const Icon(Icons.vpn_key_outlined),
                                    tooltip: 'Copy password.',
                                    onPressed: () {
                                      _copyPassword(password.password);
                                    },
                                  )
                                : TextButton(
                                    child: const Text('Copy password'),
                                    onPressed: () {
                                      _copyPassword(password.password);
                                    },
                                  ),
                          ]),
                        ),
                      ),
                    ),
                  );
                },
                childCount: filteredPasswords.length,
              ),
            );
          },
        ),
      ],
    );
  }
}

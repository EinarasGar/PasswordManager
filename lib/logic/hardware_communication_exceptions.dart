class LedgerNotFoundException implements Exception {
  LedgerNotFoundException(this.message);
  final String message;
}

class LedgerDisplayedOnScreenException implements Exception {
  LedgerDisplayedOnScreenException(this.message);
  final String message;
}

class LedgerNoSpaceLeftException implements Exception {
  LedgerNoSpaceLeftException(this.message);
  final String message;
}

class LedgerLockedException implements Exception {
  LedgerLockedException(this.message);
  final String message;
}

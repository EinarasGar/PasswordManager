import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

Future<void> CopyToClipboard(String text, BuildContext scaffoldContext, String title) async {
  await Clipboard.setData(ClipboardData(text: text));
  _eraseClipboard(text);
  ScaffoldMessenger.of(scaffoldContext).showSnackBar(SnackBar(content: Text("$title copied to clipboard")));
}

Future<void> _eraseClipboard(String previoustext) async {
  ClipboardData? data = await Clipboard.getData('text/plain');

  await Future.delayed(const Duration(seconds: 5 * 60 * 1000));
  if (data != null) {
    if (data.text == previoustext) {
      Clipboard.setData(const ClipboardData(text: ''));
    }
  }
}

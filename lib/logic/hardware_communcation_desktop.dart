import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:hid/hid.dart';
import 'package:passwordmanager/logic/hardware_communication_exceptions.dart';

class DesktopHardwareCommunication {
  Future<Uint8List> sendBytesToLedger(Uint8List bytes) async {
    final hidDevices = await getDeviceList();

    Device? nanoDevice = hidDevices.firstWhere(
        (element) => element.vendorId == 11415,
        orElse: () => throw LedgerNotFoundException('No Ledger device found'));

    await nanoDevice.open();

    nanoDevice.write(bytes);

    BytesBuilder builder = BytesBuilder();
    while (builder.isEmpty) {
      final stream = nanoDevice.read(100, 10);
      await for (var data in stream) {
        debugPrint("Stream response " + data.toString());
        builder.add(data);
      }
    }

    nanoDevice.close();
    return builder.toBytes();
  }
}

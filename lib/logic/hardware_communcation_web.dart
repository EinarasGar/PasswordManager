import 'dart:html' show EventListener;
import 'dart:js' show allowInterop;
import 'dart:js_util' show getProperty;
import 'dart:math';
import 'dart:typed_data';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:web_hid/web_hid.dart';

final ledgerDeviceIds = RequestOptionsFilter(
  vendorId: 0x2c97,
);

HidDevice? _device = null;
Uint8List? buffer = null;

Future<Uint8List> sendBytesToLedger(Uint8List bytes) async {
  if (_device == null) {
    List<HidDevice> requestDevice = await hid.requestDevice(RequestOptions(
      filters: [ledgerDeviceIds],
    ));
    if (requestDevice.isEmpty) {
      throw Exception('No Ledger device found');
    }
    _device = requestDevice[0];
    _device?.subscribeInputReport(_handleInputReport);

    debugPrint("Setting up device");
  } else {
    debugPrint("Device was already set up");
  }

  if (_device?.opened == false) {
    debugPrint("Device not opened. Opening");
    await _device?.open();
  } else {
    debugPrint("Device was already opened");
  }

  debugPrint("Sending data");
  await _device?.sendReport(0, bytes);

  while (true) {
    if (buffer != null) {
      return buffer!;
    }
    await Future.delayed(Duration(seconds: 1));
  }
}

final EventListener _handleInputReport = allowInterop((event) {
  ByteData blockData = getProperty(event, 'data');

  print(blockData.buffer.asUint8List().toString());
  buffer = blockData.buffer.asUint8List();
  if (_device?.opened == true) {
    debugPrint("Device was open so closing");

    _device?.close();
  } else {
    debugPrint("Device was not open");
  }
});

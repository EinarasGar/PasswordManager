import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:passwordmanager/logic/hardware_communication_exceptions.dart';
import 'package:tuple/tuple.dart';

import 'hardware_communcation_stub.dart' if (dart.library.js) 'hardware_communcation_web.dart' if (dart.library.io) 'hardware_communcation_io.dart';

class HardwareCommuncation {
  HardwareCommuncation();

  Future savePassword(String website, String username, String password) async {
    List<int> passwordbytes = utf8.encode(password);
    List<int> websitebytes = utf8.encode(username);
    List<int> usernamebytes = utf8.encode(website);

    BytesBuilder builder = BytesBuilder();
    builder.addByte(0xE1);
    builder.addByte(0x08);
    builder.addByte(0x00);
    builder.addByte(0x00);
    builder.addByte(password.length + website.length + username.length + 3);
    builder.addByte(passwordbytes.length);
    builder.add(passwordbytes);
    builder.addByte(websitebytes.length);
    builder.add(websitebytes);
    builder.addByte(usernamebytes.length);
    builder.add(usernamebytes);

    debugPrint(builder.toBytes().toString());

    final response = await _exchange(builder.toBytes());
    debugPrint("ENCRYPTION");
    debugPrint("RESPONSE: " + response.item1.toList().toString());
    debugPrint("SW      : " + response.item2.toString());
    return;
  }

  Future<Uint8List> encryptPassword(String password) async {
    List<int> bytes = utf8.encode(password);

    BytesBuilder builder = BytesBuilder();
    builder.addByte(0xE1);
    builder.addByte(0x07);
    builder.addByte(0x00);
    builder.addByte(0x00);
    builder.addByte(password.length);
    builder.add(bytes);

    final response = await _exchange(builder.toBytes());
    debugPrint("ENCRYPTION");
    debugPrint("RESPONSE: " + response.item1.toList().toString());
    debugPrint("SW      : " + response.item2.toString());
    return response.item1;
  }

  Future<String> decryptPassword(Uint8List passwordBytes, bool showOnDisplay) async {
    BytesBuilder builder = BytesBuilder();
    builder = BytesBuilder();
    builder.addByte(0xE1);
    builder.addByte(0x07);
    builder.addByte(0x01);
    if (showOnDisplay) {
      builder.addByte(0x01);
    } else {
      builder.addByte(0x00);
    }

    builder.addByte(passwordBytes.length);
    builder.add(passwordBytes);

    final response = await _exchange(builder.toBytes());
    debugPrint("DECRYPTION");
    debugPrint("RESPONSE: " + response.item1.toList().toString());
    debugPrint("SW      : " + response.item2.toString());
    return utf8.decode(response.item1);
  }

  Future<String> checkVersion() async {
    BytesBuilder builder = BytesBuilder();
    builder = BytesBuilder();
    builder.addByte(0xE1);
    builder.addByte(0x03);
    builder.addByte(0x00);
    builder.addByte(0x00);
    builder.addByte(0x00);

    final response = await _exchange(builder.toBytes());
    final responseList = response.item1.toList();
    debugPrint("DECRYPTION");
    debugPrint("RESPONSE: " + response.item1.toList().toString());
    debugPrint("SW      : " + response.item2.toString());
    return responseList[0].toString() + "." + responseList[1].toString() + "." + responseList[2].toString();
  }

  Future<Tuple2<Uint8List, int>> _exchange(Uint8List bytes) async {
    Uint8List wrapped = _wrapCommandAPDU(257, bytes, 64);
    Uint8List response = await sendBytesToLedger(wrapped);
    Uint8List? unwrapped = _unwrapCommandAPDU(257, response, 64);
    if (unwrapped == null) {
      //TODO: 741 is a random number need to handle it better
      return Tuple2<Uint8List, int>(Uint8List(0), 741);
    }

    Uint8List responseBytes = unwrapped.sublist(0, unwrapped.length - 2);
    Uint8List swBytes = unwrapped.sublist(unwrapped.length - 2, unwrapped.length);

    //Converts 2 bytes into int16
    int sw = ByteData.view(swBytes.buffer).getInt16(0, Endian.little);
    debugPrint("sw" + sw.toString());

    if (sw == 3155) {
      throw LedgerLockedException("Ledger is locked. Please enter pin code.");
    } else if (sw == 400) {
      throw LedgerDisplayedOnScreenException("Password is being displayed on device");
    } else if (sw == 656) {
      throw LedgerNoSpaceLeftException("No space left on the device to store password");
    }
    return Tuple2<Uint8List, int>(responseBytes, sw);
  }

  Uint8List _wrapCommandAPDU(int channel, Uint8List command, int packetSize) {
    BytesBuilder output = BytesBuilder();
    if (packetSize < 3) {
      throw Exception("Can't handle Ledger framing with less than 3 bytes for the report");
    }

    int sequenceIdx = 0;
    int offset = 0;

    //Chanel should be 1 if android doesnt work
    output.addByte((channel >> 8).toUnsigned(8));
    output.addByte(channel.toUnsigned(8));
    output.addByte(0x05);
    output.addByte((sequenceIdx >> 8).toUnsigned(8));
    output.addByte(sequenceIdx.toUnsigned(8));
    sequenceIdx++;
    output.addByte((command.length >> 8).toUnsigned(8));
    output.addByte(command.length.toUnsigned(8));
    int blockSize = (command.length > packetSize - 7 ? packetSize - 7 : command.length);
    output.add(command.sublist(offset, offset + blockSize));
    offset += blockSize;

    while (offset != command.length) {
      output.addByte((channel >> 8).toUnsigned(8));
      output.addByte(channel.toUnsigned(8));
      output.addByte(0x05);
      output.addByte((sequenceIdx >> 8).toUnsigned(8));
      output.addByte(sequenceIdx.toUnsigned(8));
      sequenceIdx++;
      blockSize = (command.length - offset > packetSize - 5 ? packetSize - 5 : command.length - offset);
      output.add(command.sublist(offset, offset + blockSize));
      offset += blockSize;
    }

    if ((output.length % packetSize) != 0) {
      Uint8List padding = Uint8List(packetSize - (output.length % packetSize));
      output.add(padding);
    }

    return output.toBytes();
  }

  Uint8List? _unwrapCommandAPDU(int channel, Uint8List data, int packetSize) {
    BytesBuilder output = BytesBuilder();

    int offset = 0;
    int responseLength;
    int sequenceIdx = 0;
    if ((data == null) || (data.length < 7 + 5)) {
      return null;
    }
    if (data[offset++] != (channel >> 8)) {
      debugPrint("Invalid channel");
    }
    if (data[offset++] != (channel & 0xff)) {
      debugPrint("Invalid channel");
    }
    if (data[offset++] != 0x05) {
      debugPrint("Invalid tag");
    }
    if (data[offset++] != 0x00) {
      debugPrint("Invalid sequence");
    }
    if (data[offset++] != 0x00) {
      debugPrint("Invalid sequence");
    }
    responseLength = ((data[offset++] & 0xff) << 8);
    responseLength |= (data[offset++] & 0xff);
    if (data.length < 7 + responseLength) {
      return null;
    }
    int blockSize = (responseLength > packetSize - 7 ? packetSize - 7 : responseLength);
    output.add(data.sublist(offset, offset + blockSize));
    offset += blockSize;
    while (output.length != responseLength) {
      sequenceIdx++;
      if (offset == data.length) {
        return null;
      }
      if (data[offset++] != (channel >> 8)) {
        debugPrint("Invalid channel");
      }
      if (data[offset++] != (channel & 0xff)) {
        debugPrint("Invalid channel");
      }
      if (data[offset++] != 0x05) {
        debugPrint("Invalid tag");
      }
      if (data[offset++] != (sequenceIdx >> 8)) {
        debugPrint("Invalid sequence");
      }
      if (data[offset++] != (sequenceIdx & 0xff)) {
        debugPrint("Invalid sequence");
      }
      blockSize = (responseLength - output.length > packetSize - 5 ? packetSize - 5 : responseLength - output.length);
      if (blockSize > data.length - offset) {
        return null;
      }
      output.add(data.sublist(offset, offset + blockSize));
      offset += blockSize;
    }
    return output.toBytes();
  }
}

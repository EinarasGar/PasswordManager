import 'dart:math';

import 'package:flutter/material.dart';
import 'package:tuple/tuple.dart';
import 'package:zxcvbn/zxcvbn.dart';

Tuple2<Color, String> getPasswordStrength(String password) {
  final zxcvbn = Zxcvbn();
  if (password.isEmpty) {
    return Tuple2(Color.fromARGB(255, 204, 51, 0), "");
  }
  final zxcvbnResult = zxcvbn.evaluate(password);
  final cracktimes = zxcvbnResult.crack_times_display;
  final crackDisplayTime = cracktimes?['offline_slow_hashing_1e4_per_second'];
  Color passwordFieldColor;
  String passwordHint;
  if (zxcvbnResult.score! == 0) {
    passwordFieldColor = const Color.fromARGB(255, 204, 51, 0);
    passwordHint = "Password is too guessable. It would take " + crackDisplayTime! + " to crack. " + zxcvbnResult.feedback.warning!;
  } else if (zxcvbnResult.score! == 1) {
    passwordFieldColor = const Color.fromARGB(255, 255, 153, 102);
    passwordHint = "Password is very guessable. It would take " + crackDisplayTime! + " to crack." + zxcvbnResult.feedback.warning!;
  } else if (zxcvbnResult.score! == 2) {
    passwordFieldColor = const Color.fromARGB(255, 255, 204, 0);
    passwordHint = "Password is somewhat guessable. It would take " + crackDisplayTime! + " to crack. " + zxcvbnResult.feedback.warning!;
  } else if (zxcvbnResult.score! == 3) {
    passwordFieldColor = const Color.fromARGB(255, 153, 204, 51);
    passwordHint = "Password is safely unguessable. It would take " + crackDisplayTime! + " to crack. " + zxcvbnResult.feedback.warning!;
  } else {
    passwordFieldColor = const Color.fromARGB(255, 51, 153, 0);
    passwordHint = "Password very unguessable. It would take " + crackDisplayTime! + " to crack. " + zxcvbnResult.feedback.warning!;
  }
  return Tuple2(passwordFieldColor, passwordHint);
}

String generateSecurePassword() {
  const characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#\$%^&*()_+{}:\"<>?|[];',./`~";
  final random = Random.secure();
  final password = StringBuffer();
  for (var i = 0; i < 16; i++) {
    password.write(characters[random.nextInt(characters.length)]);
  }
  final finalPassword = password.toString();
  final zxcvbn = Zxcvbn();
  final zxcvbnResult = zxcvbn.evaluate(finalPassword);
  if (zxcvbnResult.score == 4) {
    return finalPassword;
  } else {
    return generateSecurePassword();
  }
}

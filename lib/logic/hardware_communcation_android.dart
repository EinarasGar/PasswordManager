import 'dart:typed_data';

import 'package:flutter/services.dart';

class AndroidHardwareCommunication {
  static const platform = MethodChannel('samples.flutter.dev/battery');

  Future<Uint8List> sendBytesToLedger(Uint8List bytes) async {
    try {
      return await platform.invokeMethod('sendToLedger', bytes);
    } on PlatformException catch (e) {
      return Uint8List(0);
    }
  }
}

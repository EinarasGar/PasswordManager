import 'dart:typed_data';
import 'dart:io' show Platform;

import 'package:passwordmanager/logic/hardware_communcation_desktop.dart';

import 'hardware_communcation_android.dart';

Future<Uint8List> sendBytesToLedger(Uint8List bytes) async {
  if (Platform.isAndroid) {
    AndroidHardwareCommunication androidHardwareCommunication =
        AndroidHardwareCommunication();
    return await androidHardwareCommunication.sendBytesToLedger(bytes);
  } else if (Platform.isLinux || Platform.isWindows || Platform.isMacOS) {
    DesktopHardwareCommunication desktopHardwareCommunication =
        DesktopHardwareCommunication();

    Uint8List response =
        await desktopHardwareCommunication.sendBytesToLedger(bytes);
    return response;
  } else {
    throw UnsupportedError(
        "Platform ${Platform.operatingSystem} is not supported");
  }
}
